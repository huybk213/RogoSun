/*
 *  HuyTV14@fpt.com.vn
 */

#include "network_answer.h"
#include "min.h"
#include "common.h"
#include "app_utils.h"
#include "app_log.h"
/* Device to hub command id*/
#define RESPONSE_UPDATE_LIGHT_D_HEADER_CODE                     0xF0        // Response Daylight color
#define RESPONSE_UPDATE_LIGHT_RGB_HEADER_CODE                   0xF1        // Response RGB color
#define RESPONSE_UPDATE_LIGHT_ON_OFF_HEADER_CODE                0xF2
#define RESPONSE_UPDATE_LIGHT_ALL_STATUS_HEADER_CODE            0xF3
#define RESPONSE_REMOVAL_NODE_HEADER_CODE                       0xF5        // remove device out of mesh network
#define RESPONSE_SENSOR_TAG                                     0xF6

#define SENSOR_TYPE_TEMP_HUMI                                   0x01
#define SENSOR_TYPE_TAG                                         0x02

#define SENSOR_TYPE_TEMP_HUMI_RESPONSE_LENGTH                   0x0B
#define SENSOR_TYPE_TAG_RESPONSE_LENGTH                         0x0A

/* Device to hub command length */
#define RESPONSE_UPDATE_LIGHT_D_LENGTH                          0x05
#define RESPONSE_UPDATE_LIGHT_RGB_LENGTH                        0x06 
#define RESPONSE_UPDATE_LIGHT_ON_OFF_LENGTH                     0x04
#define RESPONSE_UPDATE_LIGHT_ALL_STATUS_LENGTH                 0x08
#define RESPONSE_REMOVAL_NODE_LENGTH                            0x03

// protocol

extern app_message_server_t m_server;   // just for testing

void networkAnswerHandle(uint8_t* data, uint8_t length)     // this function will handle all data received from mesh
{   
    /* Log for debug */
    uint8_t log_data[length*2];
    toHexArray(data, length, log_data);
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Jump to function networkAnswerHandle, length: %d\r\n",length);
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Data: 0x%s\r\n",log_data);
    /*
     * ID:
     * RESPONSE_UPDATE_LIGHT_D_HEADER_CODE
     * RESPONSE_UPDATE_LIGHT_RGB_HEADER_CODE
     * RESPONSE_UPDATE_LIGHT_ALL_STATUS_HEADER_CODE
     * RESPONSE_REMOVAL_NODE_HEADER_CODE
     */
    uint8_t id = *data; data++;
    switch (id)
    {
        case RESPONSE_UPDATE_LIGHT_D_HEADER_CODE: // 2 byte address + 2 byte on_off + + 2 byte type + 4 byte RGB + 2 byte D
        {
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "case RESPONSE_UPDATE_LIGHT_D_HEADER_CODE\r\n");
            if(length != RESPONSE_UPDATE_LIGHT_D_LENGTH)
            {
                __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "invalid length\r\n");
                return;
            }
            
            uint8_t responseWifi[12];
            uint8_t d_pwm = *data; data++;
            uint16_t node_address = *data; data++; node_address = node_address << 8;
            node_address += (*data); data++;
            
            // node address
            responseWifi[0] = node_address >> 8;
            responseWifi[1] =  node_address & 0x00FF;
            
            // state on/off
            uint16_t state_on_off = *data; data++;
            responseWifi[2] = 0;
            if(state_on_off) {responseWifi[3] = 0x02;}
            else responseWifi[3] = 0;
            
            // led type D, RGB will equal 0
            responseWifi[4] = 0; responseWifi[5] = 0;
            
            responseWifi[6] = 0; responseWifi[7] = 0; responseWifi[8] = 0; responseWifi[9] = 0;   // RGB = 0;
            
            responseWifi[10] = 0; responseWifi[11] = *data;  // D
              
            min_tx_frame(MIN_ID_RESPONSE_UPDATE_MESH_STATUS, responseWifi, 12);
            
            /* Just for debug */
            uint8_t tmp[24];
            toHexArray(responseWifi, 12, tmp);
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "MIN_TX_FRAME with ID %d, data 0x%s\r\n", MIN_ID_RESPONSE_UPDATE_MESH_STATUS, tmp);
        }
            break;

        case RESPONSE_UPDATE_LIGHT_RGB_HEADER_CODE:  // 2 byte address + 2 byte on_off + + 2 byte type + 4 byte RGB + 2 byte D
        {
            uint8_t responseWifi[12];
            uint8_t d_pwm = *data; data++;
            uint16_t node_address = *data; data++; node_address = node_address << 8;
            node_address += (*data); data++;
            
            // node address
            responseWifi[0] = node_address >> 8;
            responseWifi[1] =  node_address & 0x00FF;
            
            // state on/off
            uint16_t state_on_off = *data; data++;
            responseWifi[2] = 0;
            if(state_on_off) {responseWifi[3] = 0x02;}
            else responseWifi[3] = 0;
            
            // led type RGB
            responseWifi[4] = 0; responseWifi[5] = 1;
            
            // RGB value
            responseWifi[6] = *data; data++;
            responseWifi[7] = *data; data++;
            responseWifi[8] = 0;
            responseWifi[9] = *data;
            
            responseWifi[10] = 0; responseWifi[11] = 0;  // D = 0
              
            min_tx_frame(MIN_ID_RESPONSE_UPDATE_MESH_STATUS, responseWifi, 12);
            uint8_t tmp[24];
            toHexArray(responseWifi, 12, tmp);
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "MIN_TX_FRAME with ID %d, data 0x%s\r\n", MIN_ID_RESPONSE_UPDATE_MESH_STATUS, tmp);
        }
            break;

        case RESPONSE_UPDATE_LIGHT_ALL_STATUS_HEADER_CODE:
        {
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "case RESPONSE_UPDATE_LIGHT_ALL_STATUS_HEADER_CODE\r\n");
            if(length != RESPONSE_UPDATE_LIGHT_ALL_STATUS_LENGTH)
            {
                __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "invalid length\r\n");
                return;
            }

            // 2 byte address + 2 byte on_off + + 2 byte type + 4 byte RGB + 2 byte D
            uint8_t responseWifi[12];
            
            // 1 byte header +  2 byte address + 1 byte type + 4 byte 0xDDRRGGBB
 
            uint16_t node_address = *data; data++;  
            node_address |= *data; data++;

            uint8_t mode = *data; data++;
            
            uint8_t dd = *data; data++;
            uint8_t rr = *data; data++;
            uint8_t gg = *data; data++;
            uint8_t bb = *data; data++;


            // Answer to wireless MCU
            // 2 byte node address + 2 byte status + 2 byte type + 4 byte rgb + 2 byte dim = 12 byte
            // node address
            responseWifi[0] = node_address >> 8;
            responseWifi[1] =  node_address & 0x00FF;
            
            // state on/off
            uint16_t state_on_off = mode;
            responseWifi[2] = 0;
            if(state_on_off) {responseWifi[3] = 0x02;}      // if led is an
            else responseWifi[3] = 0;     // else led is off
            
            // led type 
            responseWifi[4] = 0;
            if(state_on_off == D_ON_RGB_OFF) { responseWifi[5] = 0;}      // D type
            else responseWifi[5] = 1; //RGB type
           
            
            // RGB value
            responseWifi[6] = rr;
            responseWifi[7] = gg;
            responseWifi[8] = 0;
            responseWifi[9] = bb;
            
            // DD value
            responseWifi[10] = 0;
            responseWifi[11] = dd; 


            min_tx_frame(MIN_ID_RESPONSE_UPDATE_MESH_STATUS, responseWifi, 12);
            uint8_t tmp[24];
            toHexArray(responseWifi, 12, tmp);
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Send data to wireless MCU 0x%s\r\n", tmp);
            break;    
        }
               
        case RESPONSE_REMOVAL_NODE_HEADER_CODE:
        {
            if(length != RESPONSE_REMOVAL_NODE_LENGTH)
            {
                __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "RESPONSE_REMOVAL_NODE_LENGTH invalid\r\n");
                return;
            }
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "RESPONSE_REMOVAL_NODE_HEADER_CODE, not implemented\r\n");
            break;
        }

        case RESPONSE_SENSOR_TAG:
        {
          if((*data) == SENSOR_TYPE_TEMP_HUMI)
           {
            if(length != SENSOR_TYPE_TEMP_HUMI_RESPONSE_LENGTH)
            {
                __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "SENSOR_TYPE_TEMP_HUMI_RESPONSE_LENGTH invalid\r\n");
                return;
            }
            data++;
            uint8_t tmp_tx[32];
            uint8_t mac[6];
            for(uint8_t i = 0; i < 6; i++)
            {
              mac[i] = *data;
              data++;
             tmp_tx[i] = mac[i];
            }
            uint8_t rssi = *data;  data++;
            uint8_t temperature_interger = *data; data++;
            uint8_t humi_interger = *data; data++;
            tmp_tx[6] = temperature_interger;
            tmp_tx[7] = humi_interger;
            tmp_tx[8] = 0;
            tmp_tx[9] = rssi;
            
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Got temperature and humidity sensor\r\n");
            uint8_t tmp[32];
            memset(tmp, 0, 32);
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "RSSI: %d\r\n", rssi);

            memset(tmp, 0, 32);
            toHexArray(mac, 6, tmp);
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "MAC: %s\r\n", tmp);
            
            memset(tmp, 0, 32);
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR,"Temperature interger: %d\r\n", temperature_interger);
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR,"Humi interger: %d\r\n", humi_interger);
      

           // min_id + 6 bytemac + 2 byte temp + humi + 2 byte rssi
           min_tx_frame(MIN_ID_SEND_TEMP_HUMI, tmp_tx, 10);
           }
          else if((*data) == SENSOR_TYPE_TAG)
          {
            if(length != SENSOR_TYPE_TAG_RESPONSE_LENGTH)
            {
                __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "SENSOR_TYPE_TEMP_TAG_RESPONSE_LENGTH invalid\r\n");
                return;
            }
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR,"Got TAG\r\n");

            data++;
            uint8_t mac[6];
            uint8_t tmp_tx[32];
            for(uint8_t i = 0; i < 6; i++)
            {
              mac[i] = *data;
              data++;
              tmp_tx[i] = mac[i];
            }
            uint8_t rssi = *data;  data++;
            bool is_detected_tag;
            is_detected_tag = *data;

            if(is_detected_tag)
            {
              __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Detected people\r\n");
              tmp_tx[6] = 0x00;
              tmp_tx[7] = 0x02;

//                         /* Message: 1 byte type + 3 byte pwm_value(0xRRGGBB) + 2 byte address which node use to answer */
//                       /* Total frame is 6 bytes */
//                       uint32_t dd = 0xFF;
//                       uint8_t tx_frame_dim[4];
//                       tx_frame_dim[0] = D_OFF_RGB_ON;
//                       tx_frame_dim[1] = dd;
//                       tx_frame_dim[2] = (0XC002 & 0xFF00) >> 8;
//                       tx_frame_dim[3] = 0XC002 & 0x00FF;
//           
//                       /* Send to mesh */
//                       publish_to_group(&m_server, 0Xc004, tx_frame_dim, 4);
            }
            else
            {
               __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR,    "Undetected people\r\n");
               tmp_tx[6] = 0x00;
               tmp_tx[7] = 0x00;


//                         /* Message: 1 byte type + 3 byte pwm_value(0xRRGGBB) + 2 byte address which node use to answer */
//                       /* Total frame is 6 bytes */
//                       uint8_t tx_frame_dim[4];
//                       tx_frame_dim[0] = ON_OFF_LIGHT_HUB_TO_DEVICE;
//                       tx_frame_dim[1] = 0x00;
//                       tx_frame_dim[2] = (0XC002 & 0xFF00) >> 8;
//                       tx_frame_dim[3] = 0XC002 & 0x00FF;
//           
//                       /* Send to mesh */
//                       publish_to_group(&m_server, 0Xc004, tx_frame_dim, 4);

            }
            tmp_tx[8] = 0x00;
            tmp_tx[9] = rssi;
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR,"RSSI: %d\r\n", rssi);

            toHexArray(mac, 6, tmp_tx);
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR,"MAC: %s\r\n", tmp_tx);
            min_tx_frame(MIN_ID_SEND_TAG, tmp_tx, 10);
            
            //hard code send back to node, just for testing 
            
             // do send to wireless mcu, not implement
          }
          break;
        }
                               
        default:
            // __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Message incomming is invalid, jump to default case\r\n");
            break;
    }
}