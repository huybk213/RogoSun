#include "app_message_server.h"
#include "app_message_common.h"
#include "log.h"
#include <stdint.h>
#include <stddef.h>

#include "access.h"
#include "nrf_mesh_assert.h"

/*****************************************************************************
 * Static functions
 *****************************************************************************/

static void reply_status(const app_message_server_t * p_server,
                         const access_message_rx_t * p_message)
{
  
    access_message_tx_t reply;
    reply.opcode.opcode = app_message_OPCODE_STATUS;
    reply.opcode.company_id = ACCESS_COMPANY_ID_NORDIC;
    reply.p_buffer = p_server->name;
    reply.length = sizeof(p_server->name);
    (void) access_model_reply(p_server->model_handle, p_message, &reply);
}


static void publish_state(app_message_server_t * p_server, bool value)
{
    access_message_tx_t msg;
    msg.opcode.opcode = app_message_OPCODE_STATUS;
    msg.opcode.company_id = ACCESS_COMPANY_ID_NORDIC;
    msg.p_buffer = p_server->name;
    msg.length = sizeof( p_server->name);
    (void) access_model_publish(p_server->model_handle, &msg);
}
/*****************************************************************************
 * Opcode handler callbacks
 *****************************************************************************/

static void rx_set_cb(access_model_handle_t handle, const access_message_rx_t * p_message, void * p_args)
{
    app_message_server_t * p_server = p_args;
    NRF_MESH_ASSERT(p_server->set_cb != NULL);
    p_server->set_cb(p_server, p_message->meta_data.src, p_message->meta_data.dst, p_message->p_data, p_message->length);
}

static void rx_status_cb(access_model_handle_t handle, const access_message_rx_t * p_message, void * p_args)
{
    app_message_server_t * p_server = p_args;
    NRF_MESH_ASSERT(p_server->get_cb != NULL);
    p_server->get_cb(p_server, p_message->meta_data.src, p_message->p_data,p_message->length);
 
}

static void rx_get_cb(access_model_handle_t handle, const access_message_rx_t * p_message, void * p_args)
{
    app_message_server_t * p_server = p_args;
    NRF_MESH_ASSERT(p_server->get_cb != NULL);
      __LOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Get--");
    reply_status(p_server, p_message);
}

static void rx_set_unreliable_cb(access_model_handle_t handle, const access_message_rx_t * p_message, void * p_args)
{
    app_message_server_t * p_server = p_args;
    NRF_MESH_ASSERT(p_server->set_cb != NULL);
    bool value = (((app_message_msg_set_unreliable_t*) p_message->p_data)->on_off) > 0;
    publish_state(p_server, value);
}

static const access_opcode_handler_t m_opcode_handlers[] =
{
    {{app_message_OPCODE_GET_STATUS,            ACCESS_COMPANY_ID_NORDIC}, rx_get_cb},
    {{app_message_OPCODE_SEND,            ACCESS_COMPANY_ID_NORDIC}, rx_set_cb},
    {{app_message_OPCODE_STATUS,            ACCESS_COMPANY_ID_NORDIC}, rx_status_cb},
    
};

/*****************************************************************************
 * Public API
 *****************************************************************************/

uint32_t app_message_server_init(app_message_server_t * p_server, uint16_t element_index)
{
    if (p_server == NULL ||
        p_server->get_cb == NULL ||
        p_server->set_cb == NULL)
    {
        return NRF_ERROR_NULL;
    }

    access_model_add_params_t init_params;
    init_params.element_index =  element_index;
    init_params.model_id.model_id = app_message_SERVER_MODEL_ID;
    init_params.model_id.company_id = ACCESS_COMPANY_ID_NORDIC;
    init_params.p_opcode_handlers = &m_opcode_handlers[0];
    init_params.opcode_count = sizeof(m_opcode_handlers) / sizeof(m_opcode_handlers[0]);
    init_params.p_args = p_server;
    init_params.publish_timeout_cb = NULL;
    return access_model_add(&init_params, &p_server->model_handle);
}

