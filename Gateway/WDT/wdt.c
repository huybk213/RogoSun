/*
 * HuyTV
 */

#include "wdt.h"

static nrf_drv_wdt_channel_id m_channel_id;
static bool is_reset = false;

// Before WDT make a reset , it run this handler.WDT events handler.
static void wdt_event_handler(void)
{
   //NOTE: The max amount of time we can spend in WDT interrupt is two cycles of 32768[Hz] clock - after that, reset occurs
   is_reset  = true;
}

void configWDT(unsigned int ms)
{
    // Configure WDT
    uint32_t err_code;
    nrf_drv_wdt_config_t config = NRF_DRV_WDT_DEAFULT_CONFIG;
    config.reload_value = ms;
    err_code = nrf_drv_wdt_init(&config, wdt_event_handler);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_drv_wdt_channel_alloc(&m_channel_id);
    APP_ERROR_CHECK(err_code);
}

void startWDT()   // run watchdog timer
{
    nrf_drv_wdt_enable();
}

void stopWDT()
{
    // cannot disable watchdog timer
}

void resetWDT()   // reset watchdog timer
{
    nrf_drv_wdt_channel_feed(m_channel_id);
}