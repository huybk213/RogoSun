/*
 * HuyTV
 */

#ifndef __WDT_H__
#define __WDT_H__

#include <stdbool.h>
#include <stdint.h>

#include "nrf.h"
#include "sdk_config.h"
#include "nrfx_wdt.h"
#include "nrf_drv_wdt.h"
#include "nrf_drv_clock.h"
#include "app_error.h"


void configWDT(unsigned int ms);    // config watchdog reset in ms-milisecond
void startWDT(); // start watchdog 
void stopWDT();  // Cannot stop watchdog
void resetWDT(); // reset watdog counter

#endif
