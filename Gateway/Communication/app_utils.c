/*
 *  HuyTV14@fpt.com.vn
 */


#include "app_utils.h"

void toHexArray(uint8_t* byte_array, uint8_t byte_array_length, uint8_t* hex_array)   // convert byte array to hex array
{
    uint8_t * pin = byte_array;
    const uint8_t * hex = "0123456789ABCDEF";
    uint8_t * pout = hex_array;
    for( ; pin < byte_array+byte_array_length; pout +=2, pin++)
    {
        pout[0] = hex[(*pin>>4) & 0xF];
        pout[1] = hex[ *pin     & 0xF];
        if (pout + 2 - hex_array > (byte_array_length*2))
        {
            /* Over flow */
            break;
        }
    }
    pout[0] = 0;
}