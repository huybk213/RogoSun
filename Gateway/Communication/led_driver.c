#include "led_driver.h"
#include "nrf_drv_pwm.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "app_timer.h"
#include "nrf_drv_clock.h"
#include "nrf.h"
#include "device_state_manager.h"
#include "flash_manager.h"
#include "log.h"

#define FLASH_CUSTOM_DATA_GROUP_ELEMENT 0x1ABC // A number in the range 0x0000 - 0x7EFF (flash_manager.h)
#define CUSTOM_DATA_FLASH_PAGE_COUNT 1

static flash_manager_t led_flash_manager; // flash manager instance 

typedef struct 
{
    uint8_t d_pwm;
    uint8_t rgb_pwm[3];           // 0xRRGGBB
    uint8_t mode;                 // mode = 0: D
                                  // mode = 1: RGB
} led_color_format_in_flash; // Format for the custom data

static nrf_drv_pwm_t m_pwm0 = NRF_DRV_PWM_INSTANCE(0);

static uint16_t const              m_pwm_top  = 255;
static uint16_t const              m_pwm_step = 1;
static nrf_pwm_values_individual_t chanel;

uint8_t r_value = 0;    //R
uint8_t g_value = 0;    //G
uint8_t b_value = 0;    //B
uint8_t d_value = 0;    //D
uint8_t current_mode = 0;

static nrf_pwm_sequence_t const    m_pwm_seq =
{
    .values.p_individual = &chanel,
    .length              = NRF_PWM_VALUES_LENGTH(chanel),
    .repeats             = 0,
    .end_delay           = 0
};

static void pwm_handler(nrf_drv_pwm_evt_type_t event_type)
{
    if (event_type == NRF_DRV_PWM_EVT_FINISHED)
    {
        chanel.channel_0 = r_value;
        chanel.channel_1 = g_value;
        chanel.channel_2 = b_value;
        chanel.channel_3 = d_value;
    }
}

void init_pwm(void)
{
    APP_ERROR_CHECK(app_timer_init());
    nrf_drv_pwm_config_t const config0 =
    {
        .output_pins =
        {
            R_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 0
            G_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 1
            B_PIN | NRF_DRV_PWM_PIN_INVERTED, // channel 2
            D_PIN | NRF_DRV_PWM_PIN_INVERTED  // channel 3
        },
        .irq_priority = APP_IRQ_PRIORITY_LOWEST,
        .base_clock   = NRF_PWM_CLK_1MHz,
        .count_mode   = NRF_PWM_MODE_UP,
        .top_value    = m_pwm_top,
        .load_mode    = NRF_PWM_LOAD_INDIVIDUAL,
        .step_mode    = NRF_PWM_STEP_AUTO
    };
    APP_ERROR_CHECK(nrf_drv_pwm_init(&m_pwm0, &config0, pwm_handler));

    chanel.channel_0 = 0;
    chanel.channel_1 = 0;
    chanel.channel_2 = 0;
    chanel.channel_3 = 0;

    (void)nrf_drv_pwm_simple_playback(&m_pwm0, &m_pwm_seq, 1,
                                      NRF_DRV_PWM_FLAG_LOOP);
}


/* This function set RGB color and save RGB value to flash  */
light_status applyRGB(uint8_t rr, uint8_t gg, uint8_t bb)
{
    if(backupRGB(rr, gg, bb) != OK) 
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "applyRGB() return ERROR\r\n");
        return ERROR;
    }
    d_value = 0;
    r_value = rr;
    g_value = gg;
    b_value = bb;
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "applyRGB() return success, current rgb value is %d %d %d\r\n", rr, gg, bb);
    return  OK;
}

/* This function set D color and save D value to flash  */
light_status applyD(uint8_t dd)
{
    if(backupD(dd) != OK)
    {   
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "applyD() return ERROR\r\n");
        return ERROR;
    }
    d_value = dd;
    r_value = 0; 
    g_value = 0; 
    b_value = 0;
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "applyD() return success, current dim value is %d\r\n", dd);
    return  OK;
}

/* This function initialize pwm driver and apply previos led color which stored in flash  */
light_status initLed(void)
{
    init_pwm();
    initFlashManagerForLed();
    if(restoreLightStatus() == UNKNOWN_MODE)
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "restoreLightStatus() return UNKNOWN_MODE\r\n");
    }
    return OK;
}

/* This function initialize flash which use when backup led's color  */
light_status initFlashManagerForLed(void)
{
    uint32_t ret_code;

    flash_manager_config_t flash_config;
    flash_config.write_complete_cb = NULL; 
    flash_config.invalidate_complete_cb = NULL; 
    flash_config.remove_complete_cb = NULL; 
    flash_config.min_available_space = WORD_SIZE;

    // The new instance of flash manager should use an unused region of flash:
    flash_config.p_area = (const flash_manager_page_t *) (((const uint8_t *) dsm_flash_area_get()) - (ACCESS_FLASH_PAGE_COUNT * PAGE_SIZE) - (NET_FLASH_PAGE_COUNT * PAGE_SIZE) );
    // The new instance of flash manager should use an unused region of flash:
    flash_config.p_area = (const flash_manager_page_t *) (((const uint8_t *) dsm_flash_area_get()) - (ACCESS_FLASH_PAGE_COUNT * PAGE_SIZE) - (NET_FLASH_PAGE_COUNT * PAGE_SIZE) );
    flash_config.page_count = CUSTOM_DATA_FLASH_PAGE_COUNT;
    ret_code = flash_manager_add(&led_flash_manager, &flash_config);
    if (NRF_SUCCESS != ret_code) 
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Flash error: no memory\r\n",ret_code);
    }
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "initFlashManagerForLed success\r\n",ret_code);
    return ERROR;

}

/* This function save led's color, mode to flash */
light_status backupAllLedStatus(uint8_t dd, uint8_t rr, uint8_t gg, uint8_t bb, uint8_t mode)
{
    // allocate flash 
    fm_entry_t * p_entry = flash_manager_entry_alloc(&led_flash_manager, FLASH_CUSTOM_DATA_GROUP_ELEMENT, sizeof(led_color_format_in_flash));
    if (p_entry == NULL)
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "backupAllLedStatus() return ERROR\r\n");
        return ERROR;
    }

    if((mode != D_OFF_RGB_ON) && (mode != D_ON_RGB_OFF) && (mode != ALL_OFF))
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "backupAllLedStatus() return ERROR\r\n");
        return ERROR;
    }

    led_color_format_in_flash * p_color = (led_color_format_in_flash *) p_entry->data;
    p_color->d_pwm = dd;
    p_color->rgb_pwm[0] = rr;
    p_color->rgb_pwm[1] = gg;
    p_color->rgb_pwm[2] = bb;
    p_color->mode = mode;
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "backupAllLedStatus() return success, current DRGB value is %d %d %d %d, mode is %d\r\n", dd, rr, gg, bb, mode);
    // write DRGB to flash
    flash_manager_entry_commit(p_entry);
    flash_manager_wait();
    current_mode = mode;
    switch (current_mode)
    {
        case D_OFF_RGB_ON:
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Set mode RGB\r\n");
            d_value = 0x00; r_value =  p_color->rgb_pwm[0]; g_value =  p_color->rgb_pwm[1]; b_value =  p_color->rgb_pwm[2]; 
            break;
        case D_ON_RGB_OFF:
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Set mode D\r\n");
            d_value = p_color->d_pwm; r_value =  0x00; g_value = 0x00; b_value = 0x00; 
            break;
        case ALL_OFF:
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Light off\r\n");
            d_value = 0x00; r_value =  0x00; g_value = 0x00; b_value = 0x00; 
            break;
        default:
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Unknow mode\r\n");
            break; 
    }
    return OK;
}

/* This function save led's D color to flash */
light_status backupD(uint8_t dd)
{
    // allocate flash 
    fm_entry_t * p_entry = flash_manager_entry_alloc(&led_flash_manager, FLASH_CUSTOM_DATA_GROUP_ELEMENT, sizeof(led_color_format_in_flash));
    if (p_entry == NULL)
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "backupDPwmValue() Error\r\n");
        return ERROR;  
    } 

    led_color_format_in_flash * p_color = (led_color_format_in_flash *) p_entry->data;
    p_color->d_pwm = dd;
    p_color->mode = D_ON_RGB_OFF;
    // write DRGB to flash
    flash_manager_entry_commit(p_entry);
    // Wait for flash manager to finish.
    flash_manager_wait();
    return OK;
}

/* This function save led's RGB color to flash */
light_status backupRGB(uint8_t rr, uint8_t gg, uint8_t bb)
{
    // allocate flash 
    fm_entry_t * p_entry = flash_manager_entry_alloc(&led_flash_manager, FLASH_CUSTOM_DATA_GROUP_ELEMENT, sizeof(led_color_format_in_flash));
    if (p_entry == NULL)
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "backupRGB() Error\r\n");
        return ERROR;  
    }

    led_color_format_in_flash * p_color = (led_color_format_in_flash *) p_entry->data;
    p_color->rgb_pwm[0] = rr;
    p_color->rgb_pwm[1] = gg;
    p_color->rgb_pwm[2] = bb;
    p_color->mode = D_OFF_RGB_ON;
    flash_manager_entry_commit(p_entry);
    // Wait for flash manager to finish.
    flash_manager_wait();
    return OK;
}

/* This function save led's mode color to flash */
light_status backupLightMode(uint8_t mode)
{
    // allocate flash 
    fm_entry_t * p_entry = flash_manager_entry_alloc(&led_flash_manager, FLASH_CUSTOM_DATA_GROUP_ELEMENT, sizeof(led_color_format_in_flash));
    if (p_entry == NULL)
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "backupLightMode() Error\r\n");
        return ERROR;  
    }

    led_color_format_in_flash * p_color = (led_color_format_in_flash *) p_entry->data;
    p_color->mode = mode;
    flash_manager_entry_commit(p_entry);
    // Wait for flash manager to finish.
    flash_manager_wait();
    return OK;
}

/* This function restore led's mode from flash */
light_status restoreLightMode(uint8_t mode)
{
    const fm_entry_t * p_read_raw = flash_manager_entry_get(&led_flash_manager, FLASH_CUSTOM_DATA_GROUP_ELEMENT);
    const led_color_format_in_flash * drgb = (const led_color_format_in_flash *) p_read_raw->data;
    current_mode = drgb->mode;
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "restoreLightMode() return %d\r\n", drgb->mode);
}

/* This function restore and apply led's staus from flash*/
light_status  restoreLightStatus()
{
    const fm_entry_t * p_read_raw = flash_manager_entry_get(&led_flash_manager, FLASH_CUSTOM_DATA_GROUP_ELEMENT);
    const led_color_format_in_flash * drgb = (const led_color_format_in_flash *) p_read_raw->data;
    current_mode = drgb->mode;
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "restoreLightStatus(), DRGB is %d %d %d %d\r\n", drgb->d_pwm, drgb->rgb_pwm[0], drgb->rgb_pwm[1], drgb->rgb_pwm[2]);
    
    switch(current_mode)
    {
        case D_ON_RGB_OFF:
        {
            r_value = 0; g_value = 0; b_value = 0;
            d_value = drgb->d_pwm;
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Light mode %d : D_ON_RGB_OFF\r\n", drgb->mode);
        }   break;
        
        case D_OFF_RGB_ON:
        {
            d_value = 0;
            r_value = drgb->rgb_pwm[0]; g_value = drgb->rgb_pwm[1]; b_value = drgb->rgb_pwm[2];
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Light mode %d : D_OFF_RGB_ON\r\n", drgb->mode);
        }   break;
        
        case ALL_OFF:
        {
            d_value = 0; r_value = 0; g_value = 0; b_value = 0;
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Light mode %d : ALL_OFF\r\n", drgb->mode);
        }   break;

        case UNKNOWN_MODE:
        {
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_ERROR, "Light mode %d : UNKNOWN_MODE\r\n", drgb->mode);
        }   break;

        default:
            break;
    }
    return OK; 
}

light_status turnOffLed(light_state on_off)
{
    if(on_off == OFF)
    {
        current_mode = ALL_OFF;
        backupLightMode(ALL_OFF);
        d_value = 0;
        r_value = 0; g_value = 0; b_value = 0;
        return OK;
    }
    else
    {
        // implement later
    }
}