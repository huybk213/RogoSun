#ifndef __USER_FUNCTION_H__
#define __USER_FUNCTION_H__

#include <stdint.h>
#include <stddef.h>

#include "app_message_server.h"
#include "app_message_common.h"

uint32_t publish_to_group(app_message_server_t * p_server, uint16_t group_address, uint8_t *data, uint8_t length);   // send data to mesh

#endif /* __USER_FUNCTION_H__ */