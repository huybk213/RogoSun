#include "min.h"
#include <stdbool.h>
#include "layer2.h"
#include "serial.h"
#include "common.h"
#include "app_log.h"
#include "user_function.h"
#include "log.h"
#include "app_message_server.h"

#include "app_utils.h"

#include "user_function.h"

extern app_message_server_t m_server;
   
volatile uint32_t uMaxValue = UINTEGER_MAXIMUM_VALUE;

uint8_t m_buf[128];
extern void node_reset(void);

static void encode_32(uint32_t data, uint8_t buf[])
{
  buf[0] = (uint8_t)((data & 0xff000000UL) >> 24);
  buf[1] = (uint8_t)((data & 0x00ff0000UL) >> 16);
  buf[2] = (uint8_t)((data & 0x0000ff00UL) >> 8);
  buf[3] = (uint8_t)(data & 0x000000ffUL);
}

static void encode_16(uint32_t data, uint8_t buf[])
{
  buf[0] = (uint8_t)((data & 0x0000ff00UL) >> 8);
  buf[1] = (uint8_t)(data & 0x000000ffUL);
}

static uint32_t decode_32(uint8_t buf[])
{
  uint32_t res;
  res = ((uint32_t)(buf[0]) << 24) | ((uint32_t)(buf[1]) << 16) | ((uint32_t)(buf[2]) << 8) | (uint32_t)(buf[3]);
  return res;
}

static uint16_t decode_16(uint8_t buf[])
{
  uint16_t res;
  res = ((uint16_t)(buf[0]) << 8) | (uint16_t)(buf[1]);
  return res;
}   

/* Macros for unpacking and packing MIN frames in various functions.
 *
 * Declaring stuff in macros like this is not pretty but it cuts down
 * on obscure errors due to typos.
 */
#define DECLARE_BUF(size)		 uint8_t m_control = (size); uint8_t m_cursor = 0
#define PACK8(v)				((m_cursor < m_control) ? m_buf[m_cursor] = (v), m_cursor++ : 0)
#define PACK16(v)				((m_cursor + 2U <= m_control) ? encode_16((v), m_buf + m_cursor), m_cursor += 2U : 0)
#define PACK32(v)				((m_cursor + 4U <= m_control) ? encode_32((v), m_buf + m_cursor), m_cursor += 4U : 0)
#define SEND_FRAME(id)			(min_tx_frame((id), m_buf, m_control))

#define DECLARE_UNPACK()		uint8_t m_cursor = 0
#define UNPACK8(v)				((m_cursor < m_control) ? ((v) = m_buf[m_cursor]), m_cursor++ : ((v) = 0))
#define UNPACK16(v)				((m_cursor + 2U <= m_control) ? ((v) = decode_16(m_buf + m_cursor)), m_cursor += 2U : ((v) = 0))
#define UNPACK32(v)				((m_cursor + 4U <= m_control) ? ((v) = decode_32(m_buf + m_cursor)), m_cursor += 4U : ((v) = 0))

/*
 * send request register to MCU
 *
 */
void do_register_read(uint8_t m_id, uint16_t addr)
{
  DECLARE_BUF(2);
  PACK16(addr);
  SEND_FRAME(MIN_ID_REGISTER_READ);
}

/*
 * do_register_write - send register changes from wifi chipset to MCU
 */

void do_register_write(uint8_t m_id, uint16_t addr,uint16_t length,uint16_t buf[])
{
  uint16_t i;
  DECLARE_BUF(length*2+4);
  PACK16(addr);
  PACK16(length);
  for ( i = 0; i < length; i++)
  {
    PACK16(buf[i]);
  }
  SEND_FRAME(MIN_ID_REGISTER_WRITE);
}

/*
 * This function send message to control ligi
 * Anwser topic: node will be answer to this address
 * target_address: hub will send message to this address
 * state_on_off: 0 = OFF, 1  = ON
 * led_type: RGB, D or ON/OFF. If type = RGB, led D will be ignore.... 
 * rgb: 0xRRGGxxBB: PWM value each color, ex 0x1122xx33 mean: PWM_R = 0x11, PWM_G = 0x22, PWM_B  = 0x33, bit 8-15 will be ignore
 * D: D_PWM
 * response: node will answer to this address
 */

static void send_light_command(uint16_t target_address, uint8_t state_on_off, uint8_t led_type, uint32_t rgb, uint8_t D, uint16_t response_address)
{
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "In function send_light_command\r\n"); 
    
    // hardcode for testing, remove this function after testing
    if(response_address > 0xc110) response_address = 0xc002;
    if(response_address < 0xc000) response_address = 0xc002;
    // state : led on or off
    if(state_on_off != LED_OFF)
    {
      
//        // hardcode for demoday
//        led_type  =  RGB_COLOR_HUB_TO_DEVICE;
//        rgb = 0x000000FF;
  
      // led_type: D mode, RGB mode, or all off
      if(led_type == DIM_COLOR_HUB_TO_DEVICE)
      {
          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "LED D ON with PWM value 0x%02x\r\n", D); 
          
          /* Message: 1 byte type + 1 byte pwm_value + 2 byte address which node use to answer */
          /* Total frame is 4 bytes */
          uint8_t tx_frame_dim[6];
          tx_frame_dim[0] = led_type;
          tx_frame_dim[1] = D;      // 8bit LSB
          tx_frame_dim[2] = (uint8_t)((response_address & 0xFF00) >> 8);
          tx_frame_dim[3] = (uint8_t)(response_address & 0x00FF);
          
          /* Send to mesh */
          publish_to_group(&m_server, target_address, tx_frame_dim, 4);
                    
          /* Log for debug */
          uint8_t temp[8];
          toHexArray(tx_frame_dim, 4, temp);
          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"Publish to group with data 0x%s\r\n", temp);
      }
      else  if(led_type == RGB_COLOR_HUB_TO_DEVICE)  // led RGB
      {
           __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "LED RGB ON\r\n");

           /* Message: 1 byte type + 3 byte pwm_value(0xRRGGBB) + 2 byte address which node use to answer */
           /* Total frame is 6 bytes */
           uint8_t tx_frame_dim[6];
           tx_frame_dim[0] = led_type;

           // 0xGGRRxxBB
           uint8_t gg = (rgb & 0xFF000000) >> 24;
           uint8_t rr = (rgb & 0x00FF0000) >> 16;
           uint8_t bb  = rgb & 0x000000FF;
           tx_frame_dim[1] = rr;
           tx_frame_dim[2] = gg;
           tx_frame_dim[3] = bb;
           tx_frame_dim[4] = (response_address & 0xFF00) >> 8;
           tx_frame_dim[5] = response_address & 0x00FF;
           
           /* Send to mesh */
           publish_to_group(&m_server, target_address, tx_frame_dim, 6);

           /*Log for debug */
           uint8_t temp[12];
           toHexArray(tx_frame_dim, 6, temp);
           __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"publish to group with data %s, target address 0x%04x, answer address 0x%04x\r\n", temp, target_address, response_address);
      }
      else __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"Invalid led type 0x%02x\r\n", led_type);
    }
    else
    {
      __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "LED OFF\r\n"); 

      /* Message: 1 byte type + 1byte all led off + 2 byte address which node use to answer */
      /* Total frame is 4 bytes */
      uint8_t tx_frame_data[4];
      tx_frame_data[0] = ON_OFF_LIGHT_HUB_TO_DEVICE;
      tx_frame_data[1] = 0x00;    // 0x00 mean all led off
      tx_frame_data[2] = (response_address & 0xFF00) >> 8;
      tx_frame_data[3] = response_address & 0x00FF;
      
      publish_to_group(&m_server,target_address, tx_frame_data, 4);

      
      /*Log for debug */
      uint8_t temp[8];
      toHexArray(tx_frame_data, 4, temp);
       __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"publish to group with data %s\r\n", temp);

      return;
    }       
}


/* Remove mesh out of mesh */

void remove_node_out_of_mesh(uint16_t target_address, uint16_t response_address)
{
        /* Message: 1 byte type  + 2 byte address which node use to answer */
      /* Total frame is 4 bytes */
      uint8_t tx_frame_data[3];
      tx_frame_data[0] = REMOVE_OUT_OF_MESH;
      tx_frame_data[1] = (response_address & 0xFF00) >> 8;
      tx_frame_data[2] = response_address & 0x00FF;
      
      publish_to_group(&m_server, target_address, tx_frame_data, 3);

      
      /*Log for debug */
      uint8_t temp[6];
      toHexArray(tx_frame_data, 3, temp);
       __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"publish to group with data %s\r\n", temp);
}



/*
 * This function send message to request node update ligit status.
 * Anwser topic: node will be answer to this address
 * Group_address: hub will send message to this address
 */

void send_request_update_status(uint16_t type, uint16_t group_address, uint16_t answer_topic)
{
     __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"In function send_request_update_status\r\n");

     uint8_t message[3] = {HUB_ASK_DEVICE_ALL_LIGHT_STATUS, 0, 0};

     message[1] = (answer_topic & 0xFF00) >> 8;
     message[2] = answer_topic & 0x00FF;

    switch(type)
    {
      case UPDATE_NODE:
      {
         __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"UPDATE_NODE status\r\n");
         publish_to_group(&m_server, group_address, message, 3);
      }
      break;

      case UPDATE_GROUP:
      {
         __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"UPDATE_GROUP status\r\n");
        publish_to_group(&m_server, group_address, message, 3);
      }
      break;

      case UPDATE_ALL:
      {
       
       /* 1 byte header + 2 byte address which node will use to answer*/
         __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"ALL status\r\n");
         publish_to_group(&m_server, group_address, message, 3);
      }

      default: __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"In function send_request_update_status(), input arg is invalid type\r\n");
      break;
    }
}


/* Do cloud request here */
void do_register_report(uint8_t m_id, uint8_t m_buf[], uint8_t m_control)
{
   /* See document Register function code.xlsx */
   // m_control is length of m_buf

   __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"In do_register_report function\r\n");
   uint8_t data[m_control*2];
   toHexArray(m_buf, m_control, data);
   __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"Data 0x%s, length %d\r\n", data, m_control);

   DECLARE_UNPACK();
 
   switch (m_id)
   {
        case MIN_ID_CONTROL_GROUP:
        case MIN_ID_CONTROL_NODE:   // also group
        {
          if(m_control != MIN_RX_CONTROL_NODE_LENTH)
          {
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"Error, invalid data lenth, length : %d,  MIN_RX_CONTROL_NODE_LENTH :%d\r\n", m_control, MIN_RX_CONTROL_NODE_LENTH);
            return;
          }
          // 2 byte target ID + 2 byte control on/off +  2 byte led_type + 4 byte rgb + 2 byte dim   + 2 byte address which node will answer  
          uint16_t target_address, state_on_off, led_type, D, response_address;
          uint32_t rgb;
          
          UNPACK16(target_address); UNPACK16(state_on_off); UNPACK16(led_type); UNPACK32(rgb); UNPACK16(D); UNPACK16(response_address);
          
          state_on_off = (state_on_off & 0x00000004) >> 2;    // refer to Register function code
          
          if(led_type != 0) led_type = 1; // D ON, RGB OFF
          else led_type = 0;    // D OFF, RGB ON
          
          send_light_command(target_address, (uint8_t)state_on_off, (uint8_t)led_type, rgb, (uint8_t)D, response_address);

          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"send_light_command, target address 0x%04x, state %d, led_type %d, D = 0x%02x, GGRRBB 0x%06x, response address =  0x%04x\r\n",
                                            target_address, state_on_off, led_type, (uint8_t)D, rgb, response_address);
          break;
        }
        
        case MIN_ID_REQUEST_UPDATE_MESH_STATUS:
        {
           // request led status
          
          uint16_t type, group_address, answer_address;
          UNPACK16(type); UNPACK16(group_address); UNPACK16(answer_address);
          send_request_update_status(UPDATE_ALL, group_address, answer_address);    // hardcode update all
        } break;

        default:
        {
          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"Invalid m_id\r\n");
          break;
        }
   }
}

/* Provides a 'ping' service by echoing back the ping frame with
 * the same ID and payload.
 */

//static void do_ping(uint8_t m_id, uint8_t m_buf[], uint8_t m_control)
//{
//	min_tx_frame(m_id, m_buf, m_control);
//}

void ping_handle(uint8_t m_id, uint8_t m_buf[], uint8_t m_control) 
{
  __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "PING........\r\n");
    // min_tx_frame(m_id, m_buf, length);
}

/*
 * Send a ping request to MIN task
 */

void ping_request(void) 
{

}

/*
 *
 */
void send_StatusRegister(void)
{

}

/*
 * send a ping frame
 */

void send_ping(void) 
{
//  DECLARE_BUF(4);
//  uint8_t i;
//  for ( i = 0; i < 4; i++)
//  {
//    PACK8(pingPayload[i]);
//  }
//  SEND_FRAME(MIN_ID_PING);
}

static void answer_wifi_host_reset(uint8_t device_type, uint16_t address)
{
  __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "answer_wifi_host_reset, not implement\r\n");
}

/* Callback from MIN layer 1 to indicate the frame has been received
 */
void min_frame_received(uint8_t buf[], uint8_t control, uint8_t id)
{
  uint8_t tmp[control*2];
  toHexArray(buf, control, tmp);
  __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "min_frame_received: 0x%s, id 0x%02x\r\n", tmp, id);
  
  switch(id) 
  {   
      case MIN_ID_CONTROL_NODE:   
          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "case MIN_ID_CONTROL_NODE........\r\n");
          do_register_report(id, buf, control);
          break;

      case MIN_ID_CONTROL_GROUP:
          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "case MIN_ID_CONTROL_GROUP........\r\n");
          do_register_report(id, buf, control);
          break;

      case MIN_ID_REGISTER_REPORT:
          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "case MIN_ID_REGISTER_REPORT........\r\n");
          break;

      case MIN_ID_REQUEST_UPDATE_MESH_STATUS:          
          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "case MIN_ID_REQUEST_UPDATE_MESH_ELEMENT_STATUS........\r\n");
          do_register_report(id,buf,control);
          break;

      case MIN_ID_REMOVE_HUB:    // remove hub from mesh network
       __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "case MIN_ID_REMOVE_HUB........implemented\r\n");
       node_reset();
      // do_reset(id,buf,control);
          break;

      case MIN_ID_REMOVE_NODE:    // remove hub from mesh network
      __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "case MIN_ID_REMOVE_NODE, implemented........\r\n");
      uint16_t target_address = buf[0]*256 | buf[1];
      uint16_t response_address = buf[2]*256 | buf[3]; 
      remove_node_out_of_mesh(target_address, response_address);

      break;

      case MIN_ID_PING:   // do nothing, not implemented, in the future, in this case, node will answer ping request from host
          ping_handle(id,buf,control);
          break;
//        case MIN_ID_REMOVE_NODE:
//        {
//          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"MIN_ID_REMOVE_NODE\r\n");
//          uint16_t type, group_address, answer_address;
//          UNPACK16(type); UNPACK16(group_address); UNPACK16(answer_address);
//          uint8_t tx_frame_data[3];
//          tx_frame_data[0] = REMOVE_OUT_OF_MESH;    // REMOVE_OUT_OF_MESH meaning remove node from mesh network
//          tx_frame_data[1] = (answer_address & 0xFF00) >> 8;
//          tx_frame_data[2] = answer_address & 0x00FF;
//          publish_to_group(&m_server, group_address, tx_frame_data, 3);
//        } break;
//
//        case MIN_ID_REMOVE_HUB:
//        {
//          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"MIN_ID_REMOVE_HUB\r\n");
//          node_reset();   // remove all mesh configuraton
//        } break;

      default:  __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Default........\r\n");
          break;
      /* Other IDs would be handled in this case statement, calling the
       * application-specific functions to unpack the frames into
       * application data for passing to the rest of the application
       */
  }

}

/* Callback from MIN to send a byte over the UART (in this example, queued in a FIFO) */
void min_tx_byte(uint8_t byte)
{
  /* Ignore FIFO overrun issue - don't send frames faster than the FIFO can handle them
   * (and make sure the FIFO is big enough to take a maximum-sized MIN frame).
   */
  uart_send(&byte, 1);

}

/* Callback from MIN to see how much transmit buffer space there is */
uint8_t min_tx_space(void)
{
  return uart_send_space();
}

void init_min(void)
{
  min_init_layer1();
}

