#ifndef __NETWORK_ANSWER_H__
#define __NETWORK_ANSWER_H__

#include <stdbool.h>
#include "flash_manager.h"
#include "user_function.h"

void networkAnswerHandle(uint8_t* data, uint8_t length);    // handle all data which received from mesh
void convertByteArray2HexArray(uint8_t* byte_array, uint16_t length, uint8_t* hex_array);   // convert byte array to hexa array

#endif /* __NETWORK_ANSWER_H__ */