#ifndef __APP_MESSAGE_COMMON_H__
#define __APP_MESSAGE_COMMON_H__

#include <stdint.h>

/**
 * @defgroup app_message_MODEL Simple Message model
 * Example model implementing basic behavior for sending on and off messages.
 * @ingroup md_examples_models_README
 * @{
 * @defgroup app_message_COMMON Common Simple Message definitions
 * @{
 */

/** Simple Message opcodes. */
typedef enum
{
    app_message_OPCODE_SEND = 0xC1,            /**< Simple Message Send. */
    app_message_OPCODE_SEND_UNRELIABLE = 0xC2,            /**< Simple Message Send Unreliable. */
    app_message_OPCODE_GET_STATUS = 0xC3, /**< Simple Message Get Status. */
    app_message_OPCODE_STATUS = 0xC4          /**< Simple Message Status. */
} app_message_opcode_t;

/** Message format for the Simple Message Set message. */
typedef struct __attribute((packed))
{
   uint8_t data[10];
} app_message_msg;

/** Message format for th Simple Message Set Unreliable message. */
typedef struct __attribute((packed))
{
    uint8_t on_off; /**< State to set. */
    uint8_t tid;    /**< Transaction number. */
} app_message_msg_set_unreliable_t;

/** Message format for the Simple Message Status message. */
typedef struct __attribute((packed))
{
    uint8_t present_on_off; /**< Current state. */
} app_message_msg_status_t;

/** @} end of app_message_COMMON */
/** @} end of app_message_MODEL */
#endif /* __APP_MESSAGE_COMMON_H__ */
