#ifndef __APP_MESSAGE_SERVER_H__
#define __APP_MESSAGE_SERVER_H__

#include <stdint.h>
#include <stdbool.h>
#include "access.h"

/**
 * @defgroup app_message_SERVER Simple Message Server
 * @ingroup app_message_MODEL
 * This module implements a simple proprietary Simple Message Server.
 * @{
 */

/** Simple Message Server model ID. */
#define app_message_SERVER_MODEL_ID (0x00)

/** Forward declaration. */
typedef struct __app_message_server app_message_server_t;

/**
 * Get callback type.
 * @param[in] p_self Pointer to the Simple Message Server context structure.
 * @returns @c true if the state is On, @c false otherwise.
 */
typedef bool (*app_message_get_cb_t)(const app_message_server_t * p_self, nrf_mesh_address_t src,  uint8_t *data, uint8_t length);

/**
 * Set callback type.
 * @param[in] p_self Pointer to the Simple Message Server context structure.
 * @param[in] on_off Desired state
 * @returns @c true if the set operation was successful, @c false otherwise.
 */
typedef bool (*app_message_set_cb_t)(const app_message_server_t * p_server, nrf_mesh_address_t src, nrf_mesh_address_t dst, uint8_t *data, uint8_t length);

/** Simple Message Server state structure. */
struct __app_message_server
{
    /** Model handle assigned to the server. */
    access_model_handle_t model_handle;
    /** Get callback. */
    app_message_get_cb_t get_cb;
    /** Set callback. */
    app_message_set_cb_t set_cb;

    uint8_t name[4];
};

/**
 * Initializes the Simple Message server.
 *
 * @note This function should only be called _once_.
 * @note The server handles the model allocation and adding.
 *
 * @param[in] p_server      Simple Message Server structure pointer.
 * @param[in] element_index Element index to add the server model.
 *
 * @retval NRF_SUCCESS         Successfully added server.
 * @retval NRF_ERROR_NULL      NULL pointer supplied to function.
 * @retval NRF_ERROR_NO_MEM    No more memory available to allocate model.
 * @retval NRF_ERROR_FORBIDDEN Multiple model instances per element is not allowed.
 * @retval NRF_ERROR_NOT_FOUND Invalid element index.
 */
uint32_t app_message_server_init(app_message_server_t * p_server, uint16_t element_index);

/** @} end of app_message_SERVER */

#endif /* __APP_MESSAGE_SERVER_H__ */
