/*
 *  HuyTV14@fpt.com.vn
 */


#include "min.h"
#include "serial.h"

SER_RING_BUF_T ser_uart_min;
static uint8_t tx_buf[TX_FIFO_MAXSIZE];
static uint8_t rx_buf[RX_FIFO_MAXSIZE];

extern void app_uart_put(uint8_t byte);

/* Send n bytes to the given USART from the given source. Returns the number of bytes actually buffered. */
uint8_t uart_send(uint8_t *src, uint8_t n)
{
        while(n--) app_uart_put(*src++);
}

/* Read up to n bytes from the given USART into the given destination. Returns the number of bytes actually read. */
uint8_t uart_receive(uint8_t *dest, uint8_t n)
{
        return 0;
}

uint8_t uart_send_space(void)
{
	return (MAX_FRAME_SIZE+1);
}


