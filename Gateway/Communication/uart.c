#include "uart.h"
#include "min.h"
#include "serial.h"

#define UART_HWFC APP_UART_FLOW_CONTROL_DISABLED
#define BLE_NUS_MAX_DATA_LEN 256
#define UART_TX_BUF_SIZE 256                         /**< UART TX buffer size. */
#define UART_RX_BUF_SIZE 256                         /**< UART RX buffer size. */

extern SER_RING_BUF_T ser_uart_min;         /* Extern serial ring buffer */


// Hanlder UART error
void uart_error_handle(app_uart_evt_t * p_event)
{
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {

    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {

    }
}


/*
 * UART Interrupt
 */
void uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[1];
    static uint8_t index = 0;
    uint32_t       err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            UNUSED_VARIABLE(app_uart_get(&data_array[0]));
            // if serial ring buffer is availble, data will be write to buffer
            if(!SER_RING_BUF_FULL(ser_uart_min))
            {
              SER_RING_BUF_WR(ser_uart_min, data_array[0]);   // wirte to buffer
            }
            break;
        
        // we will ignore error
        case APP_UART_COMMUNICATION_ERROR:
            //APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            // APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        default:
            break;
    }
}

/*
 * Init UART module
 */

void init_uart()        // 115200bps
{
//    #ifndef RX_PIN_NUMBER
//    #define RX_PIN_NUMBER  6
//    #endif
//    
//    #ifndef TX_PIN_NUMBER
//    #define TX_PIN_NUMBER  8
//    #endif

    #ifndef CTS_PIN_NUMBER
    #define CTS_PIN_NUMBER  7
    #endif
    
    #ifndef RTS_PIN_NUMBER
    #define RTS_PIN_NUMBER  5
    #endif

      // Release
    #ifndef RX_PIN_NUMBER
    #define RX_PIN_NUMBER    13
    #endif

    #ifndef TX_PIN_NUMBER
    #define TX_PIN_NUMBER  14
    #endif


    uint32_t                     err_code;
    app_uart_comm_params_t const comm_params =
    {
        .rx_pin_no    = RX_PIN_NUMBER,
        .tx_pin_no    = TX_PIN_NUMBER,
        .rts_pin_no   = RTS_PIN_NUMBER,
        .cts_pin_no   = CTS_PIN_NUMBER,
        .flow_control = APP_UART_FLOW_CONTROL_DISABLED,
        .use_parity   = false,
#if defined (UART_PRESENT)
        .baud_rate    = NRF_UART_BAUDRATE_115200
#else
        .baud_rate    = NRF_UARTE_BAUDRATE_115200
#endif
    };

    APP_UART_FIFO_INIT(&comm_params,
                       UART_RX_BUF_SIZE,
                       UART_TX_BUF_SIZE,
                       uart_event_handle,
                       APP_IRQ_PRIORITY_LOWEST,
                       err_code);
    APP_ERROR_CHECK(err_code);

    #undef  RX_PIN_NUMBER
    #undef  TX_PIN_NUMBER
    #undef  RTS_PIN_NUMBER
    #undef  CTS_PIN_NUMBER
}

//void putcUart(uint8_t data)   // send 1 byte via serial
//{
//  app_uart_put(data);
//}