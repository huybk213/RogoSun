/*
 *  HuyTV14@fpt.com.vn
 */

#ifndef __APP_UTILS_H__
#define __APP_UTILS_H__

#include <stdint.h>

void toHexArray(uint8_t* byte_array, uint8_t byte_array_length, uint8_t* hex_array);

#endif /* __APP_UTILS_H__ */