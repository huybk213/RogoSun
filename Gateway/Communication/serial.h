

#ifndef SERIAL_H_
#define SERIAL_H_

#define SER_RING_BUF_SIZE 512
#define SER_RING_BUF_RESET(serRingBuf)					(serRingBuf.rdIdx = serRingBuf.wrIdx = 0)
#define SER_RING_BUF_WR(serRingBuf, dataIn)			    (serRingBuf.data[(SER_RING_BUF_SIZE - 1) & serRingBuf.wrIdx++] = (dataIn))
#define SER_RING_BUF_RD(serRingBuf)						(serRingBuf.data[(SER_RING_BUF_SIZE - 1) & serRingBuf.rdIdx++])
#define SER_RING_BUF_EMPTY(serRingBuf)					(serRingBuf.rdIdx == serRingBuf.wrIdx)
#define SER_RING_BUF_FULL(serRingBuf)					(serRingBuf.wrIdx == ((serRingBuf.rdIdx + SER_RING_BUF_SIZE) & 0x0FFFF))
#define SER_RING_BUF_RQ_WR(serRingBuf, dataIn)	        (serRingBuf.data[(RING_BUF_SIZE - 1) & serRingBuf.wrIdx] = (dataIn))

/* Ring buffer structure */
typedef struct SER_RING_BUF_T
{
    uint8_t data[SER_RING_BUF_SIZE];
    uint16_t wrIdx;
    uint16_t rdIdx;
} SER_RING_BUF_T;

/* Select FIFO buffer sizes */
#define TX_FIFO_MAXSIZE             (70U)
#define RX_FIFO_MAXSIZE             (40U)
uint8_t init_uart_min(void);
/* Functions for application layer to use */
uint8_t uart_receive(uint8_t *dest, uint8_t n);     /* Take n bytes from receive FIFO */
uint8_t uart_send(uint8_t *src, uint8_t n);         /* Push n bytes into transmit FIFO */
uint8_t uart_send_space(void);                      /* Return how much space left in transmit FIFO */
uint8_t uart_receive_ready(void);                   /* Returns true if receive FIFO not empty */


#endif /* SERIAL_H_ */
