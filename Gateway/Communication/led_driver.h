#ifndef __LED_DRIVER_H__
#define __LED_DRIVER_H__

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#define R_PIN 17
#define G_PIN 18
#define B_PIN 19
#define D_PIN 20

// #define D_MODE 0
// #define RGB_MODE 1
// #define ALL_OFF 2

typedef enum light_state
{
  OFF = 0,
  ON = 1
} light_state;

typedef enum light_mode
{
    D_ON_RGB_OFF = 0,
    D_OFF_RGB_ON = 1,
    ALL_OFF = 2,
    STYLE = 3,
    UNKNOWN_MODE = 4
} light_mode;

typedef enum light_status
{
  ERROR = 0,
  OK = 1,
  UNKNOWN_STATUS
} light_status;

void init_pwm(void);

light_status initLed(void);
light_status initFlashManagerForLed(void);

light_status applyD(uint8_t dd);
light_status applyRGB(uint8_t rr, uint8_t gg, uint8_t bb);

light_status backupD(uint8_t d_pwm);
light_status backupRGB(uint8_t rr, uint8_t gg, uint8_t bb);
light_status backupLightMode(uint8_t mode);

light_status restoreLightMode(uint8_t mode);
light_status restoreLightStatus();

light_status turnOffLed(light_state on_off);

#endif /* __LED_DRIVER_H__ */