#include "user_function.h"
#include "log.h"
#include "access.h"
#include "nrf_mesh_assert.h"
#include "mesh_app_utils.h"
#include "access_config.h"
#include "app_log.h"

extern app_message_server_t m_server;

//uint32_t server_publish_data_to_current_group(app_message_server_t * p_server,  uint8_t *data, uint8_t length, uint8_t repeats)
//{
//    access_message_tx_t message;
//    message.opcode.opcode = app_message_OPCODE_SEND;
//    message.opcode.company_id = ACCESS_COMPANY_ID_NORDIC;
//    message.p_buffer = data;
//    message.length = length;
//   
//    uint32_t status = NRF_SUCCESS;
//    for (uint8_t i = 0; i < repeats; ++i)
//    {
//        status = access_model_publish(p_server->model_handle, &message);
//        if (status != NRF_SUCCESS)
//        {
//            break;
//        }
//    }
//    return status;
//}

uint32_t publish_to_group(app_message_server_t * p_server, uint16_t group_address, uint8_t *data, uint8_t length)
{   
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO,"\r\n\r\n1234567890\r\n\r\n");
    dsm_handle_t  handle;
    access_message_tx_t message;

    if(dsm_address_publish_add(group_address, &handle)) return 5;
    if(access_model_publish_address_set(p_server->model_handle,handle)) return 5; // 0: success, !0 : error

    message.opcode.opcode = app_message_OPCODE_SEND;
    message.opcode.company_id = ACCESS_COMPANY_ID_NORDIC;
    message.p_buffer = data;
    message.length = length;
   
    uint32_t status = NRF_SUCCESS;
    status = access_model_publish(p_server->model_handle, &message);
    if (status != NRF_SUCCESS) return status;
    dsm_address_publish_remove(group_address);
    return status;
}

//uint32_t publish_to_node(uint16_t dst_node_address,  uint8_t *data, uint8_t length)
//{
//  uint32_t status = server_publish_data_to_new_group(&m_server, dst_node_address, data, length, 1);
//  return status;
//}
