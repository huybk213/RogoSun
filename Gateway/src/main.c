#include "boards.h"
#include "simple_hal.h"
#include "log.h"
#include "access_config.h"
#include "simple_on_off_server.h"
#include "light_switch_example_common.h"
#include "mesh_app_utils.h"
#include "net_state.h"
#include "rtt_input.h"
#include "mesh_stack.h"
#include "mesh_provisionee.h"
#include "nrf_mesh_configure.h"
#include "nrf_mesh_config_examples.h"
#include "mesh_adv.h"  

#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "ble_conn_params.h"
#include "sdk_config.h"
#include "proxy.h"
#include "app_timer.h"

/* RogoSun option */
#include "app_log.h"
#include "app_message_server.h"
#include "app_message_common.h"
#include "user_function.h"
#include "app_utils.h"
#include "min.h"    /* Min protocol */
#include "uart.h"
#include "common.h"
#include "network_answer.h"
#include "serial.h"
#include "wdt.h"

/* RogoSun end option */

#define DEVICE_NAME                     "FPT Hub"
#define MIN_CONN_INTERVAL               MSEC_TO_UNITS(150,  UNIT_1_25_MS)           /**< Minimum acceptable connection interval. */
#define MAX_CONN_INTERVAL               MSEC_TO_UNITS(250,  UNIT_1_25_MS)           /**< Maximum acceptable connection interval. */
#define SLAVE_LATENCY                   0                                           /**< Slave latency. */
#define CONN_SUP_TIMEOUT                MSEC_TO_UNITS(4000, UNIT_10_MS)             /**< Connection supervisory timeout (4 seconds). */
#define FIRST_CONN_PARAMS_UPDATE_DELAY  APP_TIMER_TICKS(100)                        /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called. */
#define NEXT_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(2000)                       /**< Time between each call to sd_ble_gap_conn_param_update after the first call. */
#define MAX_CONN_PARAMS_UPDATE_COUNT    3                                           /**< Number of attempts before giving up the connection parameter negotiation. */

#define RTT_INPUT_POLL_PERIOD_MS (100)
#define LED_PIN_NUMBER (BSP_LED_0)
#define LED_PIN_MASK   (1u << LED_PIN_NUMBER)
#define LED_BLINK_INTERVAL_MS    (200)
#define LED_BLINK_CNT_START      (2)
#define LED_BLINK_CNT_RESET      (3)
#define LED_BLINK_CNT_PROV       (4)


#define FIFO_ELEMENT_SIZE 64
#define FIFO_BIGGEST_ELEMENT 128

typedef struct
{
  uint8_t buffer[FIFO_ELEMENT_SIZE];
  uint8_t length;
} data_t;

typedef struct
{
  data_t data[FIFO_BIGGEST_ELEMENT];
  uint8_t wrIdx;      // write index
  uint8_t rdIdx;      // read index 
//  uint16_t source;    // address of node which node sending data
  bool isBusy;
} rogosun_fifo_t;

bool rogo_fifo_is_empty(rogosun_fifo_t* data)
{
  if(data == NULL) return false;
  if(data->wrIdx != data->rdIdx)
  {
    return  false;
  }
  return true;
}

bool rogo_fifo_is_full(rogosun_fifo_t* data)
{
  if((data == NULL)) return false;
  if(data->wrIdx != ((data->rdIdx + FIFO_BIGGEST_ELEMENT) & 0x0FF))
  {
    return  false;
  }
  return true;
}

  // write fifo data

void rogo_fifo_write(rogosun_fifo_t* data, uint8_t* wr_data, uint8_t length)
{
  if((data == NULL) || (wr_data == NULL)) return;
  if(length >= FIFO_ELEMENT_SIZE)
  {
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Buffer input too big\r\n");
    return;
  }
  if(data->isBusy)
  {
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "rogo_fifo is busy\r\n");
    return;
  }
  if(rogo_fifo_is_full(data))
  {
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "rogo_fifo_is_full\r\n");
    return;
  }
  data->isBusy = true;
  uint8_t current_write_index = (FIFO_BIGGEST_ELEMENT-1) & data->wrIdx;
 
  memcpy(data->data[current_write_index].buffer, wr_data, length);
  data->data[current_write_index].length = length;
  data->wrIdx++;
  data->isBusy = false;
//   __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "wrIdx: %d\r\n", current_write_index);
}

void rogo_fifo_read(rogosun_fifo_t* data, data_t* rd_data)      // read fifo data
{
  if((data == NULL) || (rd_data == NULL)) return;
  if(rogo_fifo_is_empty(data))
  {
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "FIFO is empty\r\n");
    return;
  }
  data->isBusy = true;
  uint8_t current_read_index  = (FIFO_BIGGEST_ELEMENT-1) & data->rdIdx; 

  memcpy(rd_data->buffer, data->data[current_read_index].buffer, data->data[current_read_index].length);
  rd_data->length = data->data[current_read_index].length;
  
  data->rdIdx++;
  data->isBusy = false;
//  __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "rdIdx: %d\r\n", current_read_index);
}

void rogo_fifo_reset(rogosun_fifo_t* data)    // reset fifo
{
  if((data == NULL)) return;
  data->wrIdx = data->rdIdx = 0;
}

rogosun_fifo_t node_response;

char strDeviceName[20]; 
void device_name_set(void) // set DEVICE_NAME + 2 byte LSB MAC address 
{ 
  uint8_t tmp = (NRF_FICR->DEVICEADDR0%100000) & 0x000000FF;
  sprintf(&strDeviceName[0], "%s %02x\0", &DEVICE_NAME[0], tmp);    
}

static void gap_params_init(void);
static void conn_params_init(void);

static void on_sd_evt(uint32_t sd_evt, void * p_context)
{
    (void) nrf_mesh_on_sd_evt(sd_evt);
}

NRF_SDH_SOC_OBSERVER(mesh_observer, NRF_SDH_BLE_STACK_OBSERVER_PRIO, on_sd_evt, NULL);

app_message_server_t m_server;
static bool                   m_device_provisioned;


static void provisioning_complete_cb(void)
{
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Successfully provisioned\n");

    /* Restores the application parameters after switching from the Provisioning service to the Proxy  */
    gap_params_init();
    conn_params_init();

    dsm_local_unicast_address_t node_address;
    dsm_local_unicast_addresses_get(&node_address);
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Node Address: 0x%04x \n", node_address.address_start);
}

/* RogoSun Option */
static bool get_cb(const app_message_server_t * p_server, nrf_mesh_address_t src, nrf_mesh_address_t dst, uint8_t *data, uint8_t length);
static bool set_cb(const app_message_server_t * p_server, nrf_mesh_address_t src, nrf_mesh_address_t dst, uint8_t *data, uint8_t length);

/* Dont care this function */
static bool get_cb(const app_message_server_t * p_server, nrf_mesh_address_t src, nrf_mesh_address_t dst, uint8_t *data, uint8_t length)
{
   // __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Get_cb event.\n");
   return true;
}


/* Raw data coming from mesh handle here */
static bool set_cb(const app_message_server_t * p_server, nrf_mesh_address_t src, nrf_mesh_address_t dst, uint8_t *data, uint8_t length)    // event data receivered from provioner
{ 
    uint8_t tmp[length*2];
    toHexArray(data, length, tmp);
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "New data incomming \r\n");
//    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "model_handle_server : 0x%04x\r\n",p_server->model_handle);
//    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "src : 0x%04x\r\n",src.value);
//    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "dst : 0x%04x\r\n",dst.value);
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Raw data(hexa) : 0x%s, length %d\r\n", tmp, length);
    if(length == 0) 
    {
       __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Length  = 0, invalid message\r\n");
      return false;
    }
    rogo_fifo_write(&node_response, data, length);
    resetWDT();
    return true;
}
/* RogoSun end option */

void node_reset(void)   // This function remove device from mesh
{
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "----- Node reset  -----\n");
    resetWDT();   // reset watchdog
    proxy_disable();
    resetWDT();
    mesh_stack_config_clear();
    resetWDT();
    /* This function may return if there are ongoing flash operations. */
    mesh_stack_device_reset();
}

static void config_server_evt_cb(const config_server_evt_t * p_evt)
{
    if (p_evt->type == CONFIG_SERVER_EVT_NODE_RESET)
    {
        node_reset();
    }
}

unsigned char test_d = 0;
uint32_t test_rgb = 0;

extern void remove_node_out_of_mesh(uint16_t target_address, uint16_t response_address);   // just for testing

static void button_event_handler(uint32_t button_number)    // button just for testing
{
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Button %u pressed\n", button_number);
    switch (button_number)
    {
        /* Pressing SW1 on the Development Kit will result in LED state to toggle and trigger
        the STATUS message to inform client about the state change. This is a demonstration of
        state change publication due to local event. */
        case 0:
        {
          /* Message: 1 byte type + 1 byte pwm_value + 2 byte address which node use to answer */
          /* Total frame is 4 bytes */
          __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "LED RGB ON\r\n");
           uint8_t tx_frame_dim[6];
           tx_frame_dim[0] = RGB_COLOR_HUB_TO_DEVICE;
           test_rgb = 0xFF000000;
           // 0xRRGGxxBB
           tx_frame_dim[1] = (test_rgb & 0xFF000000) >> 24;
           tx_frame_dim[2] = (test_rgb & 0x00FF0000) >> 16;
           tx_frame_dim[3] = test_rgb & 0x000000FF;
           tx_frame_dim[4] = (0xc002 & 0xFF00) >> 8;
           tx_frame_dim[5] = 0xc002 & 0x00FF;
           
           /* Send to mesh */
           publish_to_group(&m_server, 0xc004, tx_frame_dim, 6);
            break;
        }
        
        case 1:
        {
           __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "LED RGB ON\r\n");

           /* Message: 1 byte type + 3 byte pwm_value(0xRRGGBB) + 2 byte address which node use to answer */
           /* Total frame is 6 bytes */
           uint8_t tx_frame_dim[6];
           tx_frame_dim[0] = RGB_COLOR_HUB_TO_DEVICE;
           test_rgb = 0x00FF0000;
           // 0xRRGGxxBB
           tx_frame_dim[1] = (test_rgb & 0xFF000000) >> 24;
           tx_frame_dim[2] = (test_rgb & 0x00FF0000) >> 16;
           tx_frame_dim[3] = test_rgb & 0x000000FF;
           tx_frame_dim[4] = (0xc002 & 0xFF00) >> 8;
           tx_frame_dim[5] = 0xc002 & 0x00FF;
           
           /* Send to mesh */
           publish_to_group(&m_server, 0xc004, tx_frame_dim, 6);
            break;
        }
        case 2:
        {
           __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "LED RGB ON\r\n");

           /* Message: 1 byte type + 3 byte pwm_value(0xRRGGBB) + 2 byte address which node use to answer */
           /* Total frame is 6 bytes */
           uint8_t tx_frame_dim[6];
           tx_frame_dim[0] = RGB_COLOR_HUB_TO_DEVICE;
           test_rgb = 0x000000FF;
           // 0xRRGGxxBB
           tx_frame_dim[1] = (test_rgb & 0xFF000000) >> 24;
           tx_frame_dim[2] = (test_rgb & 0x00FF0000) >> 16;
           tx_frame_dim[3] = test_rgb & 0x000000FF;
           tx_frame_dim[4] = (0xc002 & 0xFF00) >> 8;
           tx_frame_dim[5] = 0xc002 & 0x00FF;
           
           /* Send to mesh */
           publish_to_group(&m_server, 0xc004, tx_frame_dim, 6);
            break;
              node_reset();
              break;
        }

        /* Initiate node reset */
        case 3:
        {
           __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "LED RGB ON\r\n");

           /* Message: 1 byte type + 3 byte pwm_value(0xRRGGBB) + 2 byte address which node use to answer */
           /* Total frame is 6 bytes */
           uint8_t tx_frame_dim[6];
           tx_frame_dim[0] = RGB_COLOR_HUB_TO_DEVICE;
           test_rgb = 0x000000FF;
           // 0xRRGGxxBB
           tx_frame_dim[1] = 0x000000ed; //e900;
           tx_frame_dim[2] = 0x0000005e;
           tx_frame_dim[3] = 0x00000000;
           tx_frame_dim[4] = (0xc002 & 0xFF00) >> 8;
           tx_frame_dim[5] = 0xc002 & 0x00FF;
//           
//           /* Send to mesh */
            // remove_node_out_of_mesh(0xc001, 0xc002);
              publish_to_group(&m_server, 0xc004, tx_frame_dim, 6);
            break;
        }
        case 4:
        {
            __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "node_reset\r\n");
//            uint8_t tx_frame_data[4];
//            tx_frame_data[0] = MODE_ON_OFF_RGB_ON_D_OFF;
//             tx_frame_data[1] = MODE_ON_OFF_RGB_ON_D_OFF;
//            tx_frame_data[2] = (0xc002 & 0xFF00) >> 8;
//            tx_frame_data[3] = 0xc002 & 0x00FF;
//      
//            publish_to_group(&m_server,0xc001, tx_frame_data, 4);
//            break;
            node_reset();
            break;
        }
        case 5:
        { 
            uint8_t tmp[3] = {0x05, 0xc0, 0x02};

            publish_to_group(&m_server, 0xc004, tmp, 3);
             __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Device out of mesh\r\n");
//            node_reset();
            break;
        }
        case 6:
        {
           __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "LED D ON\r\n");

           /* Message: 1 byte type + 3 byte pwm_value(0xRRGGBB) + 2 byte address which node use to answer */
           /* Total frame is 6 bytes */
           uint8_t tx_frame_dim[6];
           tx_frame_dim[0] = DIM_COLOR_HUB_TO_DEVICE;
           test_d = 255;
           // 0xRRGGxxBB
           tx_frame_dim[1] = test_d;
//           tx_frame_dim[2] = (0xc002 & 0xFF00) >> 8;
//           tx_frame_dim[3] = 0xc002 & 0x00FF;
           tx_frame_dim[2] = (0xc002 & 0xFF00) >> 8;
           tx_frame_dim[3] = 0xc002 & 0x00FF;
           /* Send to mesh */
           publish_to_group(&m_server, 0xc004, tx_frame_dim, 4);
            break;
        }

        default:
            break;
    }
}

static void app_rtt_input_handler(int key)
{
      // didnot use this function
//    if (key >= '0' && key <= '1')
//    {
//        uint32_t button_number = key - '0';
//        button_event_handler(button_number);
//    }
}

static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    uint32_t err_code;

    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        err_code = sd_ble_gap_disconnect(p_evt->conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
        APP_ERROR_CHECK(err_code);
    }
    else if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_SUCCEEDED)
    {
        __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "Successfully updated connection parameters\n");
    }
}

static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}

static void models_init_cb(void)
{
    m_server.get_cb = get_cb;
    m_server.set_cb = set_cb;
    ERROR_CHECK(app_message_server_init(&m_server, 0));
    ERROR_CHECK(access_model_subscription_list_alloc(m_server.model_handle));
}

static void mesh_init(void)
{
    uint8_t dev_uuid[NRF_MESH_UUID_SIZE];
    uint8_t node_uuid_prefix[SERVER_NODE_UUID_PREFIX_SIZE] = SERVER_NODE_UUID_PREFIX;

    ERROR_CHECK(mesh_app_uuid_gen(dev_uuid, node_uuid_prefix, SERVER_NODE_UUID_PREFIX_SIZE));
    mesh_stack_init_params_t init_params =
    {
        .core.irq_priority       = NRF_MESH_IRQ_PRIORITY_LOWEST,
        .core.lfclksrc           = DEV_BOARD_LF_CLK_CFG,
        .core.p_uuid             = dev_uuid,
        .models.models_init_cb   = models_init_cb,
        .models.config_server_cb = config_server_evt_cb
    };
    ERROR_CHECK(mesh_stack_init(&init_params, &m_device_provisioned));
}

static void gap_params_init(void)
{
    uint32_t                err_code;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *) strDeviceName,
                                          strlen(strDeviceName));
    APP_ERROR_CHECK(err_code);
}

static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;
    ble_gap_conn_params_t  gap_conn_params;

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));
    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);

    memset(&cp_init, 0, sizeof(cp_init));
    cp_init.p_conn_params                  = &gap_conn_params;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


static void initialize(void)
{
    __LOG_INIT(LOG_SRC_APP | LOG_SRC_ACCESS, LOG_LEVEL_INFO, LOG_CALLBACK_DEFAULT);
    __MYLOG(LOG_SRC_APP, LOG_LEVEL_INFO, "RogoSun Hub\r\n");

    ERROR_CHECK(app_timer_init());
   
#if BUTTON_BOARD
//     ERROR_CHECK(hal_buttons_init(button_event_handler));
    hal_leds_init();
#endif
    uint32_t err_code = nrf_sdh_enable_request();
    APP_ERROR_CHECK(err_code);

    uint32_t ram_start = 0;
    /* Set the default configuration (as defined through sdk_config.h). */
    err_code = nrf_sdh_ble_default_cfg_set(MESH_SOFTDEVICE_CONN_CFG_TAG, &ram_start);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_sdh_ble_enable(&ram_start);
    APP_ERROR_CHECK(err_code);

    gap_params_init();
    conn_params_init();

    mesh_init();
}

static void start(void)
{
    rtt_input_enable(app_rtt_input_handler, RTT_INPUT_POLL_PERIOD_MS);
    ERROR_CHECK(mesh_stack_start());

    if (!m_device_provisioned)
    {
        static const uint8_t static_auth_data[NRF_MESH_KEY_SIZE] = STATIC_AUTH_DATA;
        mesh_provisionee_start_params_t prov_start_params =
        {
            .p_static_data    = static_auth_data,
            .prov_complete_cb = provisioning_complete_cb,
            .p_device_uri = NULL
        };
        ERROR_CHECK(mesh_provisionee_prov_start(&prov_start_params));
    }

    const uint8_t *p_uuid = nrf_mesh_configure_device_uuid_get();
//    __MYLOG_XB(LOG_SRC_APP, LOG_LEVEL_INFO, "Device UUID ", p_uuid, NRF_MESH_UUID_SIZE);
}


/* RogoSun Modify */
extern SER_RING_BUF_T ser_uart_min;
/* End */

int main(void)
  {
    
    configWDT(10000);   //
    node_response.wrIdx = 0;
    node_response.rdIdx = 0;
    node_response.isBusy = false;

    device_name_set();    // set DEVICE_NAME + LSB 2 byte MAC address
    startWDT();   // run watchdog timer
    resetWDT();   // reset watchdog timer
    initialize();
    execution_start(start);
    resetWDT();
    
    /* RogoSun modify*/
    init_uart();
    init_min();     // Init min protocol
    resetWDT();
    /* End */
    data_t tmp;     // mesh network data
    

    for (;;)
    {
//            uint32_t i = 0x000FFFFF;
//            while(i--);
//            button_event_handler(1);
            if(!rogo_fifo_is_empty(&node_response))
            {
                rogo_fifo_read(&node_response, &tmp);
                networkAnswerHandle(tmp.buffer, tmp.length);    // user processing data here
            }
            if(!SER_RING_BUF_EMPTY(ser_uart_min))
            {
              min_rx_byte(SER_RING_BUF_RD(ser_uart_min));
            }
            resetWDT();
    }
}
// nguoc d-on
  