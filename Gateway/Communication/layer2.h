/* Layer 2 definitions (application layer).
 * 
 * Defines all the MIN setup for the application, selecting IDs to be used
 * and how to pack and unpack signal data and commands.
 * 
 * Author: Ken Tindell
 * Copyright (c) 2014-2015 JK Energy Ltd.
 * Licensed under MIT License.
 */ 

#ifndef LAYER2_H_
#define LAYER2_H_

#include "common.h"

/* Command frames from the host */
#define MIN_ID_PING				(0x01U)		/* Layer 1 frame; Ping test: returns the same frame back */
#define MIN_ID_RESET			(0x05U)
#define MIN_ID_ERROR			(0x20u)

#define MIN_ID_REGISTER_WRITE	(0x30u)

#define MIN_ID_REGISTER_READ	(0x40u)
#define MIN_ID_REGISTER_REPORT	(0x41u)


#define UINTEGER_MAXIMUM_VALUE	4294967295



/* Poll the incoming uart to send bytes into MIN layer 1 */
extern void poll_rx_bytes(void);

/* Functions to take application data and send to host */
extern void do_register_read(uint8_t m_id, uint16_t addr);
extern void do_register_write(uint8_t m_id, uint16_t addr,uint16_t length,uint16_t buf[]);
extern void do_register_report(uint8_t m_id, uint8_t m_buf[], uint8_t length );
extern void send_ping(void);
extern void ping_request(void);

extern void ping_handle(uint8_t m_id, uint8_t m_buf[], uint8_t length);

extern void send_StatusRegister(void);

extern void do_report_ota_fail(void);

#endif /* LAYER2_H_ */
