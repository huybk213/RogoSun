/*
 * Author: HuyTV
 */

#ifndef __APP_LOG_H__
#define __APP_LOG_H__

#define APP_LOG_ENABLE 1

#if APP_LOG_ENABLE
#define __MYLOG(source, level, ...) __LOG(source, level, __VA_ARGS__)
#else
#define __MYLOG(source, level, ...) while(0)
#endif

#endif /* __APP_LOG_H__ */
  