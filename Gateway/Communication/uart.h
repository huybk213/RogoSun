/*
 *  HuyTV14@fpt.com.vn
 */

#ifndef __UART_H__
#define __UART_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "app_uart.h"
#include "app_error.h"
#include "nrf.h"
#include "app_util_platform.h"
#include "nrf52832_peripherals.h"

#if defined (UART_PRESENT)
#include "nrf_uart.h"
#endif
#if defined (UARTE_PRESENT)
#include "nrf_uarte.h"
#endif

void uart_error_handle(app_uart_evt_t * p_event);

void uart_event_handle(app_uart_evt_t * p_event);

void init_uart();        // 115200bps

//void putc_uart(uint8_t data);   // Send 1 byte over UART

#endif /* _UART_H__ */